# Trains: Development Assignment 3 version 3

## Problem
The problem is a vehicle routing problem. There is a graph with vertices being stations, edges being roads with a journey time. Each vertex contains a set of parcels with a weight and a destination vertex. There is a set of trains with an integer capacity, each starting on some vertex. The aim is to develop an algorithm that tells the train to deliver the parcels to their destinations.

## Solution
The main algorithm is located in _Strategy1.java_.
1. Whenever a train currently has an instruction, it will execute it.
2. Otherwise it is considered idle. The controller will look through the list of idle trains, starting from the one with the largest capacity.
3. The controller will tell it to go to the nearest vertex with a _loadable_ parcel.
Loadable here means that the weight is no more than the train capacity, and also there is a route to that vertex (the graph may not be connected).
The parcel must also not be reserved by another train.
Call the vertex v.
4. The train will look through the list of loadable parcels at v and choose the one with the furthest distance.
Call the destination vertex w.
It will compute the route from v to w. If there are any parcels wanting to go from v to somewhere along this route, the train will attempt to pick them up too.
5. Sorting this list of parcel in decreasing distance, it will go through the list and attempt to load them.
If there are ties for distances, then pick the heavier parcel.
6. The train will reserve these parcels so that other trains cannot take them.

The fastest route between two vertices is computed using Dijkstra's algorithm, as is standard.

## Motivation
1. Trains with larger capacities are more useful hence are prioritized.
1. Empty trains should strive to minimize wasted moves, hence they go to the nearest vertex with loadable parcels.
1. There is no known optimal solution for a single-depot vehicle routing problem, hence we rely on heuristics to choose which parcels to load here.
1. There is a heuristic algorithm called the Savings Algorithm, but that one assumes that trains must return to the depot at the end.
1. I didn't find any useful algorithms so I just made up my own, which is to choose the parcel with the furthest travel distance and then also load parcels that are on the way.
1. This is simple enough to understand and should deliver the parcels in a reasonable amount of time.
1. An example of where this algorithm performs poorly is if we have a graph V0--V1--V2--V3--... where parcel i starts at Vi and needs to reach V0.
1. If a train starts at V0, it will only load one parcel at a time.
1. My guess is that this algorithm performs reasonably well when there are more trains.

## Implementation

### Structure
Java is an object-oriented program so I have made classes for each object in the problem statement. 
I also made the instruction an object, e.g.
```
@0, q = Q1, load(@B) = [], moving B->A:E1, drop(@A) = [], arriving @3
```
is an object to be passed into an instruction queue in each train.
Switching the algorithm is also easy, as there is an interface called _Strategy_. 
Simply create a new class implementing _Strategy_ and pass it into the constructor of the _Controller_.
You will also need to modify the enum StrategyName to modify the factory method.

### Exceptions
When the input is not correct (e.g. negative weight, train not able to reach parcels) then exceptions are thrown.
Keeping in mind that new algorithms may be developed later, exceptions are also thrown when illegal instructions are added to the train, e.g. attempting to load a parcel that is not there.

### Testing
I've made a few simple graphs to test the algorithm. They are stored under _StrategyBasicTest_ and _Strategy1Test_.

### Javadoc
A javadoc is generated for this code.

### Potential Improvements
The strategy is currently not secure. A "cheat strategy" can be implemented by calling
```
for (Parcel p : parcels){
	p.setLocation(p.getDestination())
}
```
A potential implementation to prevent this security loophole is to pass in a defensive copy to _Strategy_.
Thus _Strategy_ will not have direct access to the real parcels or trains, but only their information.
_Strategy_ can then return a list of instructions to _Controller_.
This will take a lot more time to implement, because the reservation algorithm needs direct access to the parcels.

## How to use
Put a list of `.txt` files in the directory `src/main/resources/init/input/`.
The output will be generated in the directory `src/main/resources/init/output/`.
