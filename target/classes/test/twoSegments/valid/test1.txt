// graph V0--V1 W0--W1, which is disconnected.
4 // number of stations
V0 // station name
V1 // station name
W0 // station name
W1 // station name

2 // number of routes
EV,V0,V1,2
EW,W0,W1,3

2 // number of deliveries to be performed
PV1,V0,V1,5
PW1,W0,W1,5

2 // number of trains
QV,V1,5
QW,W1,5