package controller;

import exception.InitialConditionException;
import org.junit.jupiter.api.Test;
import strategy.StrategyName;

import static org.junit.jupiter.api.Assertions.*;

/**
 * This class tests whether Controller is able to handle invalid configurations properly.
 * It uses StrategyBasic for the tests. No strategies are actually run because errors should have been thrown before
 * the algorithm is used.
 */
public class ControllerTest {

    @Test
    /*
     * numberOfTrains <= 0.
     */
    void throwsErrorIfNoTrains() {
        String filename = "src/main/resources/test/figureEight/error/test1.txt";
        Throwable exception = assertThrows(InitialConditionException.class, () -> new Controller(filename, StrategyName.STRATEGY_BASIC));
        assertEquals("No trains were found.",exception.getMessage());
    }

    /*
     * numberOfVertices <= 1
     */
    @Test
    void throwsErrorIfThereIsOnlyOneStation() {
        String filename = "src/main/resources/test/figureEight/error/test2.txt";
        Throwable exception = assertThrows(InitialConditionException.class, () -> new Controller(filename, StrategyName.STRATEGY_BASIC));
        assertEquals("Number of stations must be at least 2.",exception.getMessage());
    }

    /*
     * Edge has an end vertex not in graph.
     */
    @Test
    void throwsErrorIfEdgesHaveIncorrectVertices() {
        String filename = "src/main/resources/test/figureEight/error/test3.txt";
        Throwable exception = assertThrows(InitialConditionException.class, () -> new Controller(filename, StrategyName.STRATEGY_BASIC));
        assertEquals("Edge E9 is incorrectly configured.",exception.getMessage());
    }

    /*
     * Parcel has either source or destination vertex not in graph.
     */
    @Test
    void throwsErrorIfParcelHasIncorrectVertices() {
        String filename = "src/main/resources/test/figureEight/error/test4.txt";
        Throwable exception = assertThrows(InitialConditionException.class, () -> new Controller(filename, StrategyName.STRATEGY_BASIC));
        assertEquals("Parcel P1 is incorrectly configured.",exception.getMessage());
    }

    /*
     * One train at V6, two parcels at V0, but one of them is too heavy so expect an exception thrown.
     */
    @Test
    void throwsErrorIfParcelUndeliverable() {
        String filename = "src/main/resources/test/figureEight/error/test5.txt";
        Throwable exception = assertThrows(InitialConditionException.class, () -> new Controller(filename, StrategyName.STRATEGY_BASIC));
        assertEquals("There is a parcel unreachable by any valid train.",exception.getMessage());
    }

    /*
     * Negative parcel weight.
     */
    @Test
    void throwsErrorIfParcelHasNegativeWeight() {
        String filename = "src/main/resources/test/figureEight/error/test6.txt";
        Throwable exception = assertThrows(InitialConditionException.class, () -> new Controller(filename, StrategyName.STRATEGY_BASIC));
        assertEquals("Parcel P2 has invalid weight.",exception.getMessage());
    }

    /*
     * Negative train capacity.
     */
    @Test
    void throwsErrorIfTrainHasNegativeCapacity() {
        String filename = "src/main/resources/test/figureEight/error/test7.txt";
        Throwable exception = assertThrows(InitialConditionException.class, () -> new Controller(filename, StrategyName.STRATEGY_BASIC));
        assertEquals("Train Q1 has negative capacity.",exception.getMessage());
    }

    /*
     * Train with capacity 100 cannot reach parcel of weight 100.
     */
    @Test
    void throwsErrorIfParcelUndeliverableTwoSegmentsVersion() {
        String filename = "src/main/resources/test/twoSegments/error/test1.txt";
        Throwable exception = assertThrows(InitialConditionException.class, () -> new Controller(filename, StrategyName.STRATEGY_BASIC));
        assertEquals("There is a parcel unreachable by any valid train.",exception.getMessage());
    }




}