package graph;

import controller.Controller;
import exception.GraphException;
import exception.InitialConditionException;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class GraphTest {
    @Test
    void canCreateEmptyGraph() {
        try {
            Graph graph = new Graph(new ArrayList<>(), new ArrayList<>());
            assertEquals(0, graph.getVertices().size());
            assertEquals(0, graph.getEdges().size());
        } catch (GraphException e) {
            e.printStackTrace();
        }
    }

    @Test
    void canCreateGraphWithOneVertex() {
        try {
            List<Vertex> vertices = Collections.singletonList(new Vertex("V1"));
            Graph graph = new Graph(vertices, new ArrayList<>());
            assertEquals(1, graph.getVertices().size());
            assertEquals(0, graph.getEdges().size());
        } catch (GraphException e) {
            e.printStackTrace();
        }
    }

    @Test
    void canCreateGraphWithTwoVertices() {
        try {
            List<Vertex> vertices = Arrays.asList(new Vertex("V1"), new Vertex("V2"));
            Graph graph = new Graph(vertices, new ArrayList<>());
            assertEquals(2, graph.getVertices().size());
            assertEquals(0, graph.getEdges().size());
        } catch (GraphException e) {
            e.printStackTrace();
        }
    }

    @Test
    void canCreateGraphWithTwoVerticesAndOneEdge() {
        try {
            Vertex v1 = new Vertex("V1");
            Vertex v2 = new Vertex("V2");
            List<Vertex> vertices = Arrays.asList(v1,v2);
            List<Edge> edges = Collections.singletonList(new Edge("E1", v1, v2, 10));
            Graph graph = new Graph(vertices, edges);
            assertEquals(2, graph.getVertices().size());
            assertEquals(1, graph.getEdges().size());
        } catch (GraphException e) {
            e.printStackTrace();
        }
    }

    @Test
    void throwsErrorIfEdgesIncorrect() {
        Vertex v1 = new Vertex("V1");
        Vertex v2 = new Vertex("V2");
        Vertex v3 = new Vertex("V3"); //not in graph
        List<Vertex> vertices = Arrays.asList(v1,v2);
        List<Edge> edges = Collections.singletonList(new Edge("E1", v1, v3, 10)); // incorrect
        Throwable exception = assertThrows(GraphException.class, () -> new Graph(vertices, edges));
        assertEquals("Vertex V3 from edge V1--V3:E1 does not exist.", exception.getMessage());
    }
}