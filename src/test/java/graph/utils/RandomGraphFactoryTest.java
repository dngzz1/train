package graph.utils;

import graph.Graph;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RandomGraphFactoryTest {
    @Test
    void canGenerateGraphWith10Vertices() {
        Graph g = RandomGraphFactory.generateGraph(10, 0);
        assertEquals(9, g.getEdges().size()); // automatically makes graph connected, so 9 edges.
    }

    @Test
    void canGenerateGraphWith10VerticesAnd10ExtraEdges() {
        Graph g = RandomGraphFactory.generateGraph(10, 10);
        assertEquals(19, g.getEdges().size());
    }

    @Test
    void failsIfNumberOfVerticesIncorrect() {
        Graph g = RandomGraphFactory.generateGraph(-1, 0);
    }
}