package graph;

import exception.GraphException;
import graph.utils.GraphFactory;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class DijkstraTest {


    @Test
    void testGraph1() {
        Graph g = GraphFactory.graphFactory1();
        Vertex source = g.getVertex("A");
        Collection<Vertex> targets = g.getVertices();
        Dijkstra d = new Dijkstra(g, source, targets);
        assertEquals(10, d.getPathLengths().get("B"));
        assertEquals(15, d.getPathLengths().get("C"));
        assertEquals(22, d.getPathLengths().get("D"));
        assertEquals(24, d.getPathLengths().get("E"));
        assertEquals(23, d.getPathLengths().get("F"));
    }

    @Test
    void testGraph2() {
        Graph g = GraphFactory.graphFactory2();
        Vertex source = g.getVertex("V0");
        Collection<Vertex> targets = g.getVertices();
        Dijkstra d = new Dijkstra(g, source, targets);
        assertEquals(2, d.getPathLengths().get("V1"));
        assertEquals(6, d.getPathLengths().get("V2"));
        assertEquals(7, d.getPathLengths().get("V3"));
        assertEquals(17, d.getPathLengths().get("V4"));
        assertEquals(22, d.getPathLengths().get("V5"));
        assertEquals(19, d.getPathLengths().get("V6"));
    }

    @Test
    void testDisconnectedGraph1() {
        Vertex v0 = new Vertex("V0");
        Vertex v1 = new Vertex("V1");
        try {
            Graph g = new Graph(Arrays.asList(v0, v1), Collections.emptyList());
            Dijkstra d = new Dijkstra(g, v0, v1);
            assertEquals(Integer.MAX_VALUE, d.getPathLengths().get("V1"));
        } catch (GraphException e) {
            e.printStackTrace();
        }

    }

    @Test
    void testDisconnectedGraph2() {
        Vertex v0 = new Vertex("V0");
        Vertex v1 = new Vertex("V1");
        Vertex v2 = new Vertex("V2");
        Edge e = new Edge("E", v0, v1, 10);
        try {
            Graph g = new Graph(Arrays.asList(v0, v1, v2), Collections.singletonList(e));
            Dijkstra d = new Dijkstra(g, v0, Arrays.asList(v1, v2));
            assertEquals(10, d.getPathLengths().get("V1"));
            assertEquals(Integer.MAX_VALUE, d.getPathLengths().get("V2"));
        } catch (GraphException ex) {
            ex.printStackTrace();
        }

    }

    @Test
    void instructionsTellYouToDoNothingIfAskingToGoToStartVertex() {
        Vertex v0 = new Vertex("V0");
        try {
            Graph g = new Graph(Collections.singletonList(v0), Collections.emptyList());
            Dijkstra d = new Dijkstra(g, v0, Collections.singletonList(v0));
            assertEquals(0, d.getInstructionsToGoTo(v0).size());
        } catch (GraphException e) {
            e.printStackTrace();
        }

    }

    @Test
    void givesNullIfNotPossibleToGoToVertex() {
        Vertex v0 = new Vertex("V0");
        Vertex v1 = new Vertex("V1");
        try {
            Graph g = new Graph(Arrays.asList(v0,v1), Collections.emptyList());
            Dijkstra d = new Dijkstra(g, v0, Arrays.asList(v0, v1));
            assertEquals(0, d.getInstructionsToGoTo(v0).size());
            assertNull(d.getInstructionsToGoTo(v1));
        } catch (GraphException e) {
            e.printStackTrace();
        }

    }

    @Test
    void instructionsToGoTo1() {
        Vertex v0 = new Vertex("V0");
        Vertex v1 = new Vertex("V1");

        Edge e  = new Edge("E", v0, v1, 2);
        try {
            Graph g = new Graph(Arrays.asList(v0, v1), Collections.singletonList(e));
            Dijkstra d = new Dijkstra(g, v0, Arrays.asList(v0, v1));
            assertEquals(1, d.getInstructionsToGoTo(v1).size());
        } catch (GraphException ex) {
            ex.printStackTrace();
        }


    }
}