package train;

import exception.InstructionSourceIncorrectException;
import exception.NoSuchParcelFoundException;
import exception.TrainOverloadException;
import graph.Edge;
import graph.Vertex;
import org.junit.jupiter.api.Test;
import graph.Parcel;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class TrainTest {
    @Test
    void trainCanTraverseFromAtoBOfLength1() {
        Vertex vertexA = new Vertex("A");
        Vertex vertexB = new Vertex("B");
        Edge edgeAB = new Edge("E1",vertexA, vertexB, 1);
        int distanceAB = edgeAB.getDistance();
        Train q1 = new Train("Q1", vertexA, 20);
        Instruction instruction = new Instruction.Builder(vertexA, edgeAB).build();
        q1.addInstructionsToQueue(Collections.singletonList(instruction));
        int time = 0;
        while(!q1.isIdle()) {
            q1.step();
            time++;
        }
        assertEquals(distanceAB, time);
        assertEquals(vertexB, q1.getLocation());
    }
    @Test
    void trainCanTraverseFromAtoBOfLength2() {
        Vertex vertexA = new Vertex("A");
        Vertex vertexB = new Vertex("B");
        Edge edgeAB = new Edge("E1",vertexA, vertexB, 2);
        int distanceAB = edgeAB.getDistance();
        Train q1 = new Train("Q1", vertexA, 20);
        Instruction instruction = new Instruction.Builder(vertexA, edgeAB).build();
        q1.addInstructionsToQueue(Collections.singletonList(instruction));
        int time = 0;
        while(!q1.isIdle()) {
            q1.step();
            time++;
        }
        assertEquals(distanceAB, time);
        assertEquals(vertexB, q1.getLocation());
    }

    @Test
    void trainCanTransportParcels() {
        Vertex vertexA = new Vertex("A");
        Vertex vertexB = new Vertex("B");
        Edge edgeAB = new Edge("E1",vertexA, vertexB, 2);
        Parcel p1 = new Parcel("P1", vertexA, vertexB, 10);
        Parcel p2 = new Parcel("P2", vertexA, vertexB, 5);
        Set<Parcel> parcels = new HashSet<>();
        parcels.add(p1);
        parcels.add(p2);
        Train q1 = new Train("Q1", vertexA, 20);
        Instruction instruction = new Instruction.Builder(vertexA, edgeAB).load(parcels).drop(parcels).build();
        q1.addInstructionsToQueue(Collections.singletonList(instruction));
        while(!q1.isIdle()) {
            q1.step();
        }
        assertEquals(vertexB, p1.getLocation());
        assertEquals(vertexB, p2.getLocation());
        assertEquals(0, q1.getParcelsOnBoard().size());
    }

    @Test
    void trainCannotOverload() {
        Vertex vertexA = new Vertex("A");
        Vertex vertexB = new Vertex("B");
        Edge edgeAB = new Edge("E1",vertexA, vertexB, 2);
        Parcel p1 = new Parcel("P1", vertexA, vertexB, 10);
        Parcel p2 = new Parcel("P2", vertexA, vertexB, 5);
        Parcel p3 = new Parcel("P3", vertexA, vertexB, 6);
        Set<Parcel> parcels = new HashSet<>();
        parcels.add(p1);
        parcels.add(p2);
        parcels.add(p3);
        Train q1 = new Train("Q1", vertexA, 20);
        Instruction instruction = new Instruction.Builder(vertexA, edgeAB).load(parcels).drop(parcels).build();
        q1.addInstructionsToQueue(Collections.singletonList(instruction));
        Throwable exception = assertThrows(TrainOverloadException.class, q1::executeInstruction);
        assertEquals("Q1: cannot load parcels", exception.getMessage());
        assertEquals(vertexA, p1.getLocation()); // parcels left untouched.
        assertEquals(vertexA, p2.getLocation());
        assertEquals(vertexA, p3.getLocation());
    }


    @Test
    void trainCanOnlyDropParcelsItOwns() {
        Vertex vertexA = new Vertex("A");
        Vertex vertexB = new Vertex("B");
        Edge edgeAB = new Edge("E1",vertexA, vertexB, 2);
        Parcel p1 = new Parcel("P1", vertexA, vertexB, 10);
        Parcel p2 = new Parcel("P2", vertexA, vertexB, 5);
        Parcel p3 = new Parcel("P3", vertexA, vertexB, 2);
        Set<Parcel> parcelsToBeLoaded = new HashSet<>();
        Set<Parcel> parcelsToBeDropped = new HashSet<>();
        parcelsToBeLoaded.add(p1);
        parcelsToBeLoaded.add(p2);
        parcelsToBeDropped.add(p3);
        Train q1 = new Train("Q1", vertexA, 20);
        Instruction instruction =
                new Instruction.Builder(vertexA, edgeAB).load(parcelsToBeLoaded).drop(parcelsToBeDropped).build();
        q1.addInstructionsToQueue(Collections.singletonList(instruction));
        Throwable exception = assertThrows(NoSuchParcelFoundException.class, () -> {
            while (!q1.isIdle()) {
                q1.executeInstruction();
            }
        });
        assertEquals("Q1: Parcel P3 is missing on board hence cannot be dropped.", exception.getMessage());
    }

    @Test
    void trainCannotLoadParcelsThatAreNotThere() {
        Vertex vertexA = new Vertex("A");
        Vertex vertexB = new Vertex("B");
        Edge edgeAB = new Edge("E1",vertexA, vertexB, 2);
        Parcel p1 = new Parcel("P1", vertexA, vertexB, 10);
        Parcel p2 = new Parcel("P2", vertexB, vertexA, 10); // not there
        Set<Parcel> parcelsToBeLoaded = new HashSet<>();
        parcelsToBeLoaded.add(p1);
        parcelsToBeLoaded.add(p2);
        Train q1 = new Train("Q1", vertexA, 20);
        Instruction instruction =
                new Instruction.Builder(vertexA, edgeAB).load(parcelsToBeLoaded).build();
        q1.addInstructionsToQueue(Collections.singletonList(instruction));
        Throwable exception = assertThrows(NoSuchParcelFoundException.class, () -> {
            while (!q1.isIdle()) {
                q1.executeInstruction();
            }
        });
        assertEquals("Q1: Parcel P2 not found at location A", exception.getMessage());
    }

    @Test
    void trainCannotExecuteInstructionIfSourceLocationIsWrong() {
        Vertex vertexA = new Vertex("A");
        Vertex vertexB = new Vertex("B");
        Edge edgeAB = new Edge("E1",vertexA, vertexB, 2);
        Train q1 = new Train("Q1", vertexB, 20); // train is at vertex B, cannot run instruction.
        Instruction instruction =
                new Instruction.Builder(vertexA, edgeAB).build();
        q1.addInstructionsToQueue(Collections.singletonList(instruction));
        Throwable exception = assertThrows(InstructionSourceIncorrectException.class, () -> {
            while (!q1.isIdle()) {
                q1.executeInstruction();
            }
        });
        assertEquals("Q1: Instruction source is A but I am at B", exception.getMessage());
    }

    @Test
    void trainCanExecuteASequenceOfInstructions() {
        /*
         * This is a graph A--B--C--D with @A q1, p1, p2, p3.
         * q1 delivers p1 to B, p2 to C and ends up at D with p3 still on board (not dropped).
         */
        Vertex vertexA = new Vertex("A");
        Vertex vertexB = new Vertex("B");
        Vertex vertexC = new Vertex("C");
        Vertex vertexD = new Vertex("D");
        int distanceAB = 2;
        int distanceBC = 3;
        int distanceCD = 4;
        Edge edgeAB = new Edge("E1",vertexA, vertexB, distanceAB);
        Edge edgeBC = new Edge("E2",vertexB, vertexC, distanceBC);
        Edge edgeCD = new Edge("E3", vertexC, vertexD, distanceCD);
        Train q1 = new Train("Q1", vertexA, 20);
        Parcel p1 = new Parcel("P1", vertexA, vertexB, 1);
        Parcel p2 = new Parcel("P2", vertexA, vertexC, 1);
        Parcel p3 = new Parcel("P3", vertexA, vertexD, 1);
        Set<Parcel> parcelsToBeLoadedAtA = new HashSet<>();
        parcelsToBeLoadedAtA.add(p1);
        parcelsToBeLoadedAtA.add(p2);
        parcelsToBeLoadedAtA.add(p3);
        Set<Parcel> parcelsToBeDroppedAtB = new HashSet<>();
        parcelsToBeDroppedAtB.add(p1);
        Set<Parcel> parcelsToBeDroppedAtC = new HashSet<>();
        parcelsToBeDroppedAtC.add(p2);
        Instruction instruction1 =
                new Instruction.Builder(vertexA, edgeAB)
                        .load(parcelsToBeLoadedAtA)
                        .drop(parcelsToBeDroppedAtB)
                        .build();
        Instruction instruction2 =
                new Instruction.Builder(vertexB, edgeBC)
                        .drop(parcelsToBeDroppedAtC)
                        .build();
        Instruction instruction3 = new Instruction.Builder(vertexC, edgeCD).build();
        q1.addInstructionsToQueue(Arrays.asList(instruction1, instruction2, instruction3));
        int time = 0;
        while(!q1.isIdle()) {
            q1.step();
            time++;
        }
        assertEquals(distanceAB + distanceBC + distanceCD, time);
        assertEquals(vertexD, q1.getLocation());
        assertEquals(vertexB, p1.getLocation());
        assertEquals(vertexC, p2.getLocation());
        assertEquals(q1, p3.getLocation());
    }
}