package strategy;

import controller.Controller;
import exception.InitialConditionException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Strategy1Test {
    /*
     * One train, one parcel.
     */
    @Test
    void oneTrainOneParcel() {
        String filename = "src/main/resources/test/figureEight/valid/test1.txt";
        try {
            Controller controller = new Controller(filename, StrategyName.STRATEGY1);
            controller.run();
            assertEquals(38, controller.getTime());
        } catch (InitialConditionException e) {
            e.printStackTrace();
        }
    }



    /*
     * One train at V6, two parcels at V0.
     */
    @Test
    void oneTrainTwoParcelsAtSameLocation() {
        String filename = "src/main/resources/test/figureEight/valid/test2.txt";
        try {
            Controller controller = new Controller(filename, StrategyName.STRATEGY1);
            controller.run();
            assertEquals(76, controller.getTime());
        } catch (InitialConditionException e) {
            e.printStackTrace();
        }
    }

    /*
     * 0 parcels. Nothing to be done.
     */
    @Test
    void noParcels() {
        String filename = "src/main/resources/test/figureEight/valid/test3.txt";
        try {
            Controller controller = new Controller(filename, StrategyName.STRATEGY1);
            controller.run();
            assertEquals(0, controller.getTime());
        } catch (InitialConditionException e) {
            e.printStackTrace();
        }
    }

    /*
     *  3 trains at V6, 1 parcel at V0, but only one train has enough capacity to deliver the parcel.
     */
    @Test
    void threeTrainsOneParcelButOnlyOneTrainHasEnoughCapacity() {
        String filename = "src/main/resources/test/figureEight/valid/test4.txt";
        try {
            Controller controller = new Controller(filename, StrategyName.STRATEGY1);
            controller.run();
            assertEquals(38, controller.getTime());
        } catch (InitialConditionException e) {
            e.printStackTrace();
        }
    }

    /*
     * 3 trains at V6, 1 parcel at V0, will they fight to get it?
     */
    @Test
    void threeTrainsOneParcelAndAllTrainsHaveEnoughCapacity() {
        String filename = "src/main/resources/test/figureEight/valid/test5.txt";
        try {
            Controller controller = new Controller(filename, StrategyName.STRATEGY1);
            controller.run();
            assertEquals(38, controller.getTime());
        } catch (InitialConditionException e) {
            e.printStackTrace();
        }
    }

    /*
     * 3 trains at V6, 2 parcels at V0.
     */
    @Test
    void threeTrainsTwoParcels() {
        String filename = "src/main/resources/test/figureEight/valid/test6.txt";
        try {
            Controller controller = new Controller(filename, StrategyName.STRATEGY1);
            controller.run();
            assertEquals(38, controller.getTime());
        } catch (InitialConditionException e) {
            e.printStackTrace();
        }
    }




    /*
     * simple delivery on two segments, V0--V1 (length 2), W0--W1 (length 3). There is a parcel PV at V0, PW at W0 to
     *  be delivered to the
     * vertices V1 and W1 respectively. There are trains at V1 and W1.
     */
    @Test
    void testTwoSegments1() {
        String filename = "src/main/resources/test/twoSegments/valid/test1.txt";
        try {
            Controller controller = new Controller(filename, StrategyName.STRATEGY1);
            controller.run();
            assertEquals(6, controller.getTime());
        } catch (InitialConditionException e) {
            e.printStackTrace();
        }
    }

    /* Graph =

    (V0)-----E1:1------(V1)------E3:10--------(V2)
                         |                    |
                         |                    |
                        E2:1                 E4:1
                         |                    |
                         |                    |
                       (W1)                  (W2)
    at V0: Q1 capacity 10
       P1: V1->W1, weight 5
       P2: V2->W2, weight 5
     */
    @Test
    void testLetterF1() {
        String filename = "src/main/resources/test/letterF/valid/test1.txt";
        try {
            Controller controller = new Controller(filename, StrategyName.STRATEGY1);
            controller.run();
            assertEquals(14, controller.getTime());
        } catch (InitialConditionException e) {
            e.printStackTrace();
        }
    }
    /* Graph =

        (V0)-----E1:1------(V1)------E3:10--------(V2)
                             |                    |
                             |                    |
                            E2:1                 E4:1
                             |                    |
                             |                    |
                           (W1)                  (W2)
        at V0: Q1 capacity 10
           P1: V0->V1, weight 5
           P2: V0->V2, weight 5
 */
    @Test
    void testLetterF2() {
        String filename = "src/main/resources/test/letterF/valid/test2.txt";
        try {
            Controller controller = new Controller(filename, StrategyName.STRATEGY1);
            controller.run();
            assertEquals(11, controller.getTime());
        } catch (InitialConditionException e) {
            e.printStackTrace();
        }
    }


    /* Graph =

    (V0)-----E1:1------(V1)------E3:10--------(V2)
                         |                    |
                         |                    |
                        E2:1                 E4:1
                         |                    |
                         |                    |
                       (W1)                  (W2)
    at V1: Q1 capacity 5
    at V2: Q2 capacity 10
       P0: V1->V0, weight 2
       P1: V1->V2, weight 4
       P2: V1->V2, weight 6
     */
    @Test
    void testLetterF3() {
        String filename = "src/main/resources/test/letterF/valid/test3.txt";
        try {
            Controller controller = new Controller(filename, StrategyName.STRATEGY1);
            controller.run();
            assertEquals(10, controller.getTime());
        } catch (InitialConditionException e) {
            e.printStackTrace();
        }
    }
}