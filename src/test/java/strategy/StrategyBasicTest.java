package strategy;

import controller.Controller;
import exception.InitialConditionException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class StrategyBasicTest {
    /*
     * One train, one parcel.
     */
    @Test
    void oneTrainOneParcel() {
        String filename = "src/main/resources/test/figureEight/valid/test1.txt";
        try {
            Controller controller = new Controller(filename, StrategyName.STRATEGY_BASIC);
            controller.run();
            assertEquals(38, controller.getTime());
        } catch (InitialConditionException e) {
            e.printStackTrace();
        }
    }



    /*
     * One train at V6, two parcels at V0.
     */
    @Test
    void oneTrainTwoParcelsAtSameLocation() {
        String filename = "src/main/resources/test/figureEight/valid/test2.txt";
        try {
            Controller controller = new Controller(filename, StrategyName.STRATEGY_BASIC);
            controller.run();
            assertEquals(76, controller.getTime());
        } catch (InitialConditionException e) {
            e.printStackTrace();
        }
    }

    /*
     * 0 parcels. Nothing to be done.
     */
    @Test
    void noParcels() {
        String filename = "src/main/resources/test/figureEight/valid/test3.txt";
        try {
            Controller controller = new Controller(filename, StrategyName.STRATEGY_BASIC);
            controller.run();
            assertEquals(0, controller.getTime());
        } catch (InitialConditionException e) {
            e.printStackTrace();
        }
    }

    /*
     *  3 trains at V6, 1 parcel at V0, but only one train has enough capacity to deliver the parcel.
     */
    @Test
    void threeTrainsOneParcelButOnlyOneTrainHasEnoughCapacity() {
        String filename = "src/main/resources/test/figureEight/valid/test4.txt";
        try {
            Controller controller = new Controller(filename, StrategyName.STRATEGY_BASIC);
            controller.run();
            assertEquals(38, controller.getTime());
        } catch (InitialConditionException e) {
            e.printStackTrace();
        }
    }

    /*
     * 3 trains at V6, 1 parcel at V0, will they fight to get it?
     */
    @Test
    void threeTrainsOneParcelAndAllTrainsHaveEnoughCapacity() {
        String filename = "src/main/resources/test/figureEight/valid/test5.txt";
        try {
            Controller controller = new Controller(filename, StrategyName.STRATEGY_BASIC);
            controller.run();
            assertEquals(38, controller.getTime());
        } catch (InitialConditionException e) {
            e.printStackTrace();
        }
    }

    /*
     * 3 trains at V6, 2 parcels at V0.
     */
    @Test
    void threeTrainsTwoParcels() {
        String filename = "src/main/resources/test/figureEight/valid/test6.txt";
        try {
            Controller controller = new Controller(filename, StrategyName.STRATEGY_BASIC);
            controller.run();
            assertEquals(38, controller.getTime());
        } catch (InitialConditionException e) {
            e.printStackTrace();
        }
    }

    /*
     * 3 trains at V6, 1 parcel at V0->V6, 1 parcel at V1 -> V6.
     */
    @Test
    void threeTrainsTwoParcelsAtDifferentLocation() {
        String filename = "src/main/resources/test/figureEight/valid/test7.txt";
        try {
            Controller controller = new Controller(filename, StrategyName.STRATEGY_BASIC);
            controller.run();
            assertEquals(38, controller.getTime());
        } catch (InitialConditionException e) {
            e.printStackTrace();
        }
    }


    /*
     * simple delivery on two segments, V0--V1 (length 2), W0--W1 (length 3). There is a parcel PV at V0, PW at W0 to
     *  be delivered to the
     * vertices V1 and W1 respectively. There are trains at V1 and W1.
     */
    @Test
    void testTwoSegments1() {
        String filename = "src/main/resources/test/twoSegments/valid/test1.txt";
        try {
            Controller controller = new Controller(filename, StrategyName.STRATEGY_BASIC);
            controller.run();
            assertEquals(6, controller.getTime());
        } catch (InitialConditionException e) {
            e.printStackTrace();
        }
    }

}