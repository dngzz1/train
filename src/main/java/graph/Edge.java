
package graph;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
/**
 * <h1>Edge data structure</h1>
 * An edge, as currently implemented, is bidirectional, so the source and target entries are irrelevant.
 * The only relevant variable is the set {source, target}.
 */
public class Edge implements Location {
    private final String name;
    private final Vertex source;
    private final Vertex target;
    private final Set<Vertex> ends = new HashSet<>();
    private final int distance;

    public Edge(String name, Vertex source, Vertex target, int distance) {
        this.name = name;
        this.source = source;
        this.target = target;
        this.distance = distance;
        ends.add(source);
        ends.add(target);
    }

    public Set<Vertex> getEnds() {
        return new HashSet<>(ends);
    }

    /**
     * Given a vertex, returns the other vertex.
     * @param vertex an input vertex
     * @return the other vertex of this edge
     */
    public Vertex getOtherEnd(Vertex vertex) {
        if (vertex.equals(source)) {
            return target;
        } else if (vertex.equals(target)) {
            return source;
        } else {
            System.out.println("this edge does not have the given vertex");
            return null;
        }
    }

    public int getDistance() {
        return distance;
    }

    public String getName() {
        return name;
    }

    /**
     * A utility method to compute the total distance of a list of edges.
     * @param edges a list of edges.
     * @return the total distance as an integer.
     */
    public static int getTotalDistance(List<Edge> edges) {
        int total = 0;
        for (Edge edge : edges) {
            total += edge.getDistance();
        }
        return total;
    }

    @Override
    public String toString() {
        return source + "--" + target + ":" + name;
    }
}
