package graph;

import exception.GraphException;

import java.util.*;

/**
 * <h1>Graph data structure</h1>
 * The default constructor creates an empty graph.
 * After creating the graph, use methods to add the vertices and edges.
 * The addEdge method does <i>not</i> check whether the ends of the edges are vertices of the graph.
 * An automatic data structure adjEdges is generated, which keeps track of the emanating edges of each vertex.
 * @author Daniel Ng
 */
public class Graph {
    private final Map<String, Vertex> verticesMap = new HashMap<>();
    private final Map<String, Edge> edgesMap = new HashMap<>();
    private final Map<String, List<Edge>> adjEdges = new HashMap<>();

    /**
     * Creates a graph with a given collection of vertices and edges.
     * @param vertices a collection of vertices to be added.
     * @param edges a collection of edges to be added.
     * @throws GraphException if the ends of the edges are not in the collection of vertices.
     */
    public Graph(Collection<Vertex> vertices, Collection<Edge> edges) throws GraphException {
        for (Vertex vertex : vertices) {
            verticesMap.put(vertex.getName(), vertex);
            adjEdges.put(vertex.getName(), new ArrayList<>());
        }
        for (Edge edge : edges) {
            edgesMap.put(edge.getName(), edge);
            for (Vertex vertex : edge.getEnds()) { // loop of size 2.
                String vertexName = vertex.getName();
                if (!adjEdges.containsKey(vertexName)) {
                    throw new GraphException("Vertex " + vertexName + " from edge " + edge + " does not exist.");
                }
                adjEdges.get(vertexName).add(edge);
            }
        }
    }

    // Getters.

    /**
     * Returns the collection of edges in the graph.
     * @return the collection of edges in the graph.
     */
    public Collection<Edge> getEdges() {
        return edgesMap.values();
    }

    /**
     * Returns the vertex object based on the vertex's name.
     * @param name the name of the vertex
     * @return the vertex object
     */
    public Vertex getVertex(String name) {
        return verticesMap.get(name);
    }

    /**
     * Returns the collection of vertices in the graph.
     * @return the collection of vertices in the graph.
     */
    public Collection<Vertex> getVertices() {
        return verticesMap.values();
    }

    /**
     * Returns the list of edges emanating from a vertex.
     * @param vertex the vertex object
     * @return a list of edges emanating from this vertex.
     */
    public List<Edge> getAdjEdges(Vertex vertex) {
        return adjEdges.get(vertex.getName());
    }

    @Override
    public String toString() {
        return "Graph{" +
                "vertices=" + verticesMap.values() +
                ", edges=" + edgesMap.values() +
                '}';
    }
}
