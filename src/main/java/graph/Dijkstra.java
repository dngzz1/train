package graph;

import train.Instruction;

import java.util.*;

/**
 * Dijkstra -- an algorithm to calculate the distance of a given node
 * to all other points.
 * It is more efficient to call Dijkstra once for a set of target vertices (targets)
 * than to call Dijkstra multiple times for each vertex separately (target).
 * @author Daniel Ng
 * We will run this only for (start, destination) for each parcel.
 * https://www.baeldung.com/java-dijkstra
 */
public class Dijkstra {
    private final Vertex start;
    private final Collection<Vertex> targets;
    private final Map<String, Integer> distance = new HashMap<>();
    private final Map<String, List<Edge>> shortestPath = new HashMap<>();
    private final Map<String, List<Edge>> shortestRelevantPath = new HashMap<>();


    /**
     * Dijkstra will run the algorithm when it is instantiated.
     * @param graph the graph in question
     * @param start the starting vertex
     * @param targets the list of vertices to calculate distances from the starting vertex
     */
    public Dijkstra(Graph graph, Vertex start, Collection<Vertex> targets) {
        this.start = start;
        this.targets = targets;

        for (Vertex vertex : graph.getVertices()) {
            distance.put(vertex.getName(), Integer.MAX_VALUE);
        }
        distance.put(start.getName(), 0);
        shortestPath.put(start.getName(), new ArrayList<>()); // shortest path from start to start is length 0.
        // 2. add start vertex to the set of unsettled vertices.
        Set<Vertex> unsettledVertices = new HashSet<>();
        unsettledVertices.add(start);
        // 3. evaluation.
        Set<Vertex> settledVertices = new HashSet<>();
        while (!settledVertices.containsAll(targets) && unsettledVertices.size() > 0) {
            Vertex currentVertex = getLowestDistanceVertex(unsettledVertices);
            unsettledVertices.remove(currentVertex);
            for (Edge edge : graph.getAdjEdges(currentVertex)) {
                Vertex adjVertex = edge.getOtherEnd(currentVertex);
                if (!settledVertices.contains(adjVertex)) {
                    calculateMinimumDistance(adjVertex, edge, currentVertex);
                    unsettledVertices.add(adjVertex);
                }
            }
            settledVertices.add(currentVertex);
        }
        // restrict the map to only targets.
        for (Vertex vertex : targets) {
            String vertexName = vertex.getName();
            if (shortestPath.containsKey(vertexName)) {
                shortestRelevantPath.put(vertexName, shortestPath.get(vertexName));
            } else if (vertex.equals(start)) {
                shortestRelevantPath.put(vertexName, new ArrayList<>());
            } else {
                shortestRelevantPath.put(vertexName, null); // not possible to get there.
            }
        }
    }

    /**
     * Overloading the constructor. If targets contains only one vertex, then use this method.
     * It is advisable to use the original constructor when dealing with multiple targets.
     * @param graph the graph in question
     * @param start the starting vertex
     * @param target the target vertex
     */
    public Dijkstra(Graph graph, Vertex start, Vertex target) {
        this(graph, start, new HashSet<>(Collections.singletonList(target)));
    }


    /**
     * A helper method.
     * Returns the vertex with the lowest distance among a set of vertices.
     * It uses distance: a map from string to integer.
     * @param unsettledVertices set of vertices
     * @return the vertex with the lowest distance from the start vertex.
     */
    private Vertex getLowestDistanceVertex(Set<Vertex> unsettledVertices) {
        Vertex lowestDistanceVertex = null;
        int lowestDistance = Integer.MAX_VALUE;
        for (Vertex vertex : unsettledVertices) {
            int nodeDistance = distance.get(vertex.getName());
            if (nodeDistance < lowestDistance) {
                lowestDistance = nodeDistance;
                lowestDistanceVertex = vertex;
            }
        }
        return lowestDistanceVertex;
    }

    /**
     * A helper method.
     * Updates the map shortestPath for evaluationVertex if a shorter distance is found.
     * @param evaluationVertex attempt to look for shorter distance for this vertex
     * @param edge must connect evaluationVertex with sourceVertex
     * @param sourceVertex this vertex is settled already
     */
    private void calculateMinimumDistance(Vertex evaluationVertex,
                                         Edge edge,
                                         Vertex sourceVertex) {
        Integer sourceDistance = distance.get(sourceVertex.getName());
        Integer evaluationDistance = distance.get(evaluationVertex.getName());
        if (sourceDistance + edge.getDistance() < evaluationDistance) {
            distance.put(evaluationVertex.getName(), sourceDistance + edge.getDistance());
            List<Edge> s = new ArrayList<>(shortestPath.get(sourceVertex.getName()));
            s.add(edge);
            shortestPath.put(evaluationVertex.getName(), s);
        }
    }

    /**
     * Generates a list of instructions, readable by trains, to move from the start vertex (as given to Dijkstra) to
     * a target vertex. If the target vertex is the same as the start vertex, an array list of size 0 will be
     * returned. If the target vertex has not been computed by Dijkstra, then null will be returned.
     * @param target the target vertex, or the destination.
     * @return a list of instructions readable by trains.
     */
    public List<Instruction> getInstructionsToGoTo(Vertex target) {
        List<Instruction> result = new ArrayList<>();
        if (target.equals(start)) {
            return result;
        }
        if (!shortestRelevantPath.containsKey(target.getName())) {
            return null; // shortest distance not calculated by this algorithm.
        }
        if (shortestRelevantPath.get(target.getName()) == null) {
            return null; // not possible to reach target.
        }
        List<Edge> path = shortestRelevantPath.get(target.getName());
        result.add(new Instruction.Builder(start, path.get(0)).build());
        Vertex next = path.get(0).getOtherEnd(start);
        for (int i = 1; i < path.size(); i++) {
            result.add(new Instruction.Builder(next, path.get(i)).build());
            next = path.get(i).getOtherEnd(next);
        }
        return result;
    }

    /**
     * This gives the desired path lengths for each of the target vertices.
     * @return a map indexed by vertexName:String with values being the distance:Integer.
     * The key is the set of targets plus the start vertex.
     * If a target vertex is unreachable from the start vertex, the value is Integer.MAX_VALUE.
     */
    public Map<String, Integer> getPathLengths() {
        Map<String, Integer> map = new HashMap<>();
        for (Vertex v : targets) {
            if (shortestRelevantPath.get(v.getName()) != null) {
                map.put(v.getName(), Edge.getTotalDistance(shortestRelevantPath.get(v.getName())));
            }
            else {
                map.put(v.getName(), Integer.MAX_VALUE);
            }
        }
        return map;
    }

    @Override
    public String toString() {
        return "Dijkstra{" +
                "shortestPath=" + shortestPath +
                '}';
    }
}
