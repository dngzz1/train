package graph.utils;

import exception.GraphException;
import graph.Edge;
import graph.Graph;
import graph.Vertex;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.RandomAccess;
import java.util.concurrent.ThreadLocalRandom;


public class RandomGraphFactory {
    /**
     * Generates a random graph with a given number of vertices (n).
     * Add (int) (prob * (n choose 2)) number of edges.
     * Graph may have multiple edges between the same two nodes but asymptotically this does not matter.
     * Graph is connected by ensuring V(i) is connected to some V(j), j less than i.
     * @param numberOfVertices the number of vertices
     * @param numberOfEdges the number of edges
     * @return graph
     */
    public static Graph generateGraph(int numberOfVertices, int numberOfEdges) {
        List<Vertex> vertices = new ArrayList<>();
        List<Edge> edges = new ArrayList<>();
        int edgeId = 0;
        for (int i = 0; i < numberOfVertices; i++) {
            vertices.add(new Vertex("V" + i));
        }
        // connect V(i) to some V(j), j < i
        for (int i = 1; i < numberOfVertices; i++) {
            Vertex vi = vertices.get(i);
            int j = ThreadLocalRandom.current().nextInt(0, i);
            Vertex vj = vertices.get(j);
            int weight = (int) (Math.random() * 100);
            edges.add(new Edge("E" + (edgeId++), vi, vj, weight));
        }
        // add in the extra edges randomly.
        for (int i = 0; i < numberOfEdges; i++) {
            Vertex v1 = getRandomElement(vertices);
            Vertex v2 = getRandomElement(vertices);
            int weight = (int) (Math.random() * 100);
            edges.add(new Edge("E" + (edgeId++), v1, v2, weight));
        }
        Graph g = null;
        try {
            g = new Graph(vertices, edges);
        } catch (GraphException e) {
            e.printStackTrace();
        }
        return g;
    }

    /**
     * @author https://stackoverflow.com/questions/21092086/get-random-element-from-collection
     * @param collection the collection (set, list etc.) to get random elements from
     * @param <E> a generic type
     * @return a random element
     */
    public static <E> E getRandomElement(Collection<E> collection)
    {
        if(collection.isEmpty())
            return null;

        int randomIndex = ThreadLocalRandom.current().nextInt(collection.size());

        if(collection instanceof RandomAccess)
        {
            List<E> list = (List<E>) collection;

            return list.get(randomIndex);
        }
        else
        {
            for(E element : collection)
            {
                if(randomIndex == 0)
                    return element;

                randomIndex--;
            }
            return null; //unreachable
        }
    }
}
