package graph.utils;

import exception.GraphException;
import graph.Edge;
import graph.Graph;
import graph.Vertex;

import java.util.Arrays;

public class GraphFactory {
    public static Graph graphFactory1() {
        // from https://www.baeldung.com/java-dijkstra
        Vertex vertexA = new Vertex("A");
        Vertex vertexB = new Vertex("B");
        Vertex vertexC = new Vertex("C");
        Vertex vertexD = new Vertex("D");
        Vertex vertexE = new Vertex("E");
        Vertex vertexF = new Vertex("F");
        Graph g = null;
        try {
            g = new Graph(Arrays.asList(vertexA,vertexB,vertexC,vertexD,vertexE,vertexF),
                    Arrays.asList(
                            new Edge("AB",vertexA, vertexB, 10),
                            new Edge("AC", vertexA, vertexC, 15),
                            new Edge("BD", vertexB, vertexD, 12),
                            new Edge("BF", vertexB, vertexF, 15),
                            new Edge("CE", vertexC, vertexE, 10),
                            new Edge("DE", vertexD, vertexE, 2),
                            new Edge("DF", vertexD, vertexF, 1),
                            new Edge("FE", vertexF, vertexE, 5)
                    ));
            return g;
        } catch (GraphException e) {
            e.printStackTrace();
        }
        return g;
    }

    public static Graph graphFactory2() {
        // figure of 8 graph from
        // https://www.freecodecamp.org/news/dijkstras-shortest-path-algorithm-visual-introduction/
        Vertex vertex0 = new Vertex("V0");
        Vertex vertex1 = new Vertex("V1");
        Vertex vertex2 = new Vertex("V2");
        Vertex vertex3 = new Vertex("V3");
        Vertex vertex4 = new Vertex("V4");
        Vertex vertex5 = new Vertex("V5");
        Vertex vertex6 = new Vertex("V6");
        Graph g = null;
        try {
            g = new Graph(Arrays.asList(vertex0,vertex1,vertex2,vertex3,vertex4,vertex5,vertex6),
                    Arrays.asList(
                            new Edge("E01",vertex0, vertex1, 2),
                            new Edge("E02",vertex0, vertex2, 6),
                            new Edge("E13",vertex1, vertex3, 5),
                            new Edge("E23",vertex2, vertex3, 8),
                            new Edge("E35",vertex3, vertex5, 15),
                            new Edge("E34",vertex3, vertex4, 10),
                            new Edge("E45",vertex4, vertex5, 6),
                            new Edge("E46",vertex4, vertex6, 2),
                            new Edge("E56",vertex5, vertex6, 6)

                    ));
        } catch (GraphException e) {
            e.printStackTrace();
        }
        return g;
    }
}
