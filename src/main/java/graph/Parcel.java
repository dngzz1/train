package graph;

import train.Train;

import java.util.HashSet;
import java.util.Set;

/**
 * <h1>Parcel data structure</h1>
 * A parcel contains the following basic information:
 * <ul>
 *     <li>its name</li>
 *     <li>its location (can be on a vertex or on a train)</li>
 *     <li>its destination vertex</li>
 *     <li>its weight</li>
 * </ul>
 * The following computations are made:
 * <ul>
 *     <li>its distance to destination. The value is null if the two vertices are not path-connected by the graph.
 *     This is calculated by the Controller class.</li>
 *     <li>a list of reachable trains. This is initialized by the Checker class.</li>
 * </ul>
 */
public class Parcel {
    private final String name;
    private Location location;
    private final Vertex destination;
    private final int weight;
    private Integer distanceToDestination = null;
    private Train reservedFor = null;
    private final Set<Train> validTrains = new HashSet<>();

    /**
     * Constructor
     * @param name name of the parcel
     * @param location the initial location of the parcel. This should be a vertex. The parcel may later be moved
     *                 onto a train, hence why the type is of Location.
     * @param destination the destination vertex
     * @param weight a positive integer weight.
     */
    public Parcel(String name, Location location, Vertex destination, int weight) {
        this.name = name;
        this.location = location;
        this.destination = destination;
        this.weight = weight;
    }

    /**
     * Check if the parcel has been delivered.
     * @return true if parcel location is not the parcel destination.
     */
    public boolean stillNotDelivered() {
        if (!(location instanceof Vertex)) {
            return true;
        }
        return !destination.equals(location);
    }

    /**
     * Ensures train is not overloaded. Trains are also not allowed to take parcels reserved for someone else.
     * @param q the train which is attempting to load the parcel.
     * @return false if:
     * 1. loading the parcel results in overloading the train
     * 2. parcel has been reserved by someone else
     * This is a dynamic result which is different from isReachableBy(q).
     */
    public boolean canBeLoadedBy(Train q) {
        if (q.getWeightOfParcelsOnBoard() + getWeight() > q.getCapacity()) {
            return false;
        }
        return reservedFor == null || reservedFor.equals(q);
    }

    /**
     * Check if train q is able to load this parcel. It checks for path-connectivity and capacity.
     * It is different from canBeLoadedBy(q) because this can be checked before the trains run.
     * @param q the train
     * @return true if train q can load this parcel.
     */
    public boolean isReachableBy(Train q) {
        return validTrains.contains(q);
    }

    // getters.

    /**
     * Returns the destination vertex.
     * @return the destination vertex
     */
    public Vertex getDestination() {
        return destination;
    }

    /**
     * Returns the distance from its original location to its destination. Its value does not get updated when
     * it has been moved. The value is null if the destination is not reachable.
     * @return the distance from the source to the destination.
     */
    public Integer getDistanceToDestination() {
        return distanceToDestination;
    }

    /**
     * Returns the location of the parcel, which can be on a vertex or a train.
     * @return the location of the parcel, which can be on a vertex or a train.
     */
    public Location getLocation() {
        return location;
    }

    /**
     * Returns the name of the parcel.
     * @return the name of the parcel.
     */
    public String getName() {
        return name;
    }

    /**
     * Returns the trains which have the capacity and route to get to this parcel.
     * @return the trains which have the capacity and route to get to this parcel.
     */
    public Set<Train> getValidTrains() {
        return validTrains;
    }

    /**
     * Returns the weight of the parcel.
     * @return the weight of the parcel.
     */
    public int getWeight() {
        return weight;
    }

    // setters.

    /**
     * Called when the data is loaded.
     * @param distanceToDestination an integer. This should really only be accessed by the controller/checker.
     */
    public void setDistanceToDestination(Integer distanceToDestination) {
        this.distanceToDestination = distanceToDestination;
    }


    /**
     * This is basically a setter for the reservation.
     * @param train the train which is requesting a reservation.
     */
    public void reserveFor(Train train) {
        this.reservedFor = train;
    }

    /**
     * This method should only be called when parcels are being loaded or dropped.
     * @param location the location of the parcel (can be a vertex or a train).
     */
    public void setLocation(Location location) {
        this.location = location;
    }

    @Override
    public String toString() {
        return name;
    }
}
