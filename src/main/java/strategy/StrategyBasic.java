package strategy;

import graph.Dijkstra;
import graph.Graph;
import graph.Parcel;
import graph.Vertex;
import train.Instruction;
import train.Train;

import java.util.*;
import java.util.stream.Collectors;

/**
 * <h1>A minimal viable strategy to get all parcels to its destination</h1>
 * This strategy produces the following algorithm:
 * <ol>
 *     <li>Sort the list of idle trains with smaller trains at the top of the list.</li>
 *     <li>For each train in this list, if there is a feasible job, then add the instructions to go the nearest vertex
 *     with loadable parcels. This can be the current vertex, in which case the instruction is a list of size 0.
 *     Don't execute this instruction just yet.</li>
 *     <li>Reserve parcels at that vertex using the reserving algorithm.</li>
 * </ol>
 * The reserving algorithm does this:
 * <ol>
 *     <li>Get a list of loadable parcels at this vertex.</li>
 *     <li>Reserve the parcel with the closest distance.</li>
 *     <li>Add the instruction to deliver this parcel. </li>
 * </ol>
 * This algorithm is clearly suboptimal as the train will only be able to travel with one parcel at a time. But it
 * will get the job done.
 */
public class StrategyBasic implements Strategy{
    private final Graph graph;
    private final List<Parcel> parcels;
    private final List<Train> trains;

    public StrategyBasic(Graph graph, List<Parcel> parcels, List<Train> trains) {
        this.graph = graph;
        this.parcels = parcels;
        this.trains = trains;
    }

    @Override
    public void assignJobs() {
        // get list of idle trains. Trains with smaller capacity prioritized.
        List<Train> idleTrains = trains.stream()
                .filter(Train::isIdle)
                .sorted(Comparator.comparingInt(Train::getCapacity))
                .collect(Collectors.toList());
        for (Train q : idleTrains) {
            List<Parcel> loadableParcels = q.getLoadableParcels(parcels);
            if (loadableParcels.size() == 0) continue; // this train does nothing.
            Set<Vertex> viableDestinations = loadableParcels.stream()
                    .map(Parcel::getLocation)
                    .map(v -> (Vertex) v)
                    .collect(Collectors.toSet());
            Dijkstra d = new Dijkstra(graph, (Vertex) q.getLocation(), viableDestinations);
            Vertex closestDestination =
                    graph.getVertex(Collections.min(d.getPathLengths().entrySet(),
                            Map.Entry.comparingByValue()).getKey());
            List<Instruction> instructions = d.getInstructionsToGoTo(closestDestination);
            q.addInstructionsToQueue(instructions);
            reserveParcelsAtVertex(closestDestination, q, loadableParcels); // reserve parcels.
        }
    }


    private void reserveParcelsAtVertex(Vertex vertex, Train q, List<Parcel> availableParcels)  {
        List<Parcel> availableParcelsSorted = availableParcels.stream()
                .filter(p -> p.getLocation().equals(vertex))
                .filter(Parcel::stillNotDelivered)
                .filter(p -> p.canBeLoadedBy(q))
                .sorted(Comparator.comparingInt(Parcel::getDistanceToDestination)) // closest distance
                .collect(Collectors.toList());
        if (availableParcelsSorted.size() == 0) {
            return;
        }
        // Take the parcel with the furthest destination.
        Set<Parcel> parcelsToBePickedUp = new HashSet<>();
        Parcel furthestParcel = availableParcelsSorted.get(0);
        parcelsToBePickedUp.add(furthestParcel);
        // compute the instructions to deliver this furthestParcel.
        Vertex source = (Vertex) furthestParcel.getLocation();
        Vertex destination = furthestParcel.getDestination();
        Dijkstra d = new Dijkstra(graph, source, destination);
        List<Instruction> instructions = d.getInstructionsToGoTo(destination);
        instructions.get(0).getLoad().add(furthestParcel);
        instructions.get(instructions.size() - 1).getDrop().add(furthestParcel);
        q.addInstructionsToQueue(instructions);
        for (Parcel p : parcelsToBePickedUp) {
            p.reserveFor(q);
        }
    }
}
