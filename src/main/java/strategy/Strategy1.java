package strategy;

import graph.*;
import train.Instruction;
import train.Train;

import java.util.*;
import java.util.stream.Collectors;

/**
 * <h1>Strategy 1</h1>
 * In this strategy, large trains are prioritized.
 * When trains are idle, they go to the nearest vertex (call it v) with an undelivered parcel within their capacity
 * (valid = undelivered, weight at most their capacity).
 * Let the list of valid parcels at v be L.
 * Within v, they take the parcel with the furthest distance to travel.
 * They compute this route r.
 * Let L' be the sublist of L whose parcels have destinations lying somewhere along this route r.
 * Sort L' by distance in decreasing order.
 * For parcels with tied distances, sort by weight in decreasing order.
 * Go through this list and load the parcels if possible.
 */
public class Strategy1 implements Strategy{
    private final Graph graph;
    private final List<Parcel> parcels;
    private final List<Train> trains;

    /**
     * Constructor.
     * @param graph the graph in question
     * @param parcels the list of parcels in question
     * @param trains the list of trains
     */
    public Strategy1(Graph graph, List<Parcel> parcels, List<Train> trains) {
        this.graph = graph;
        this.parcels = parcels;
        this.trains = trains;
    }

    @Override
    public void assignJobs() {
        List<Train> idleTrains = trains.stream()
                .filter(Train::isIdle)
                .sorted(Comparator.comparingInt(Train::getCapacity).reversed()) // prioritize large trains.
                .collect(Collectors.toList());
        for (Train q : idleTrains) {
            List<Parcel> loadableParcels = q.getLoadableParcels(parcels);
            if (loadableParcels.size() == 0) continue; // this train does nothing.
            Set<Vertex> viableDestinations = loadableParcels.stream()
                    .map(Parcel::getLocation)
                    .map(v -> (Vertex) v)
                    .collect(Collectors.toSet());
            Dijkstra d = new Dijkstra(graph, (Vertex) q.getLocation(), viableDestinations);
            Vertex closestDestination =
                    graph.getVertex(Collections.min(d.getPathLengths().entrySet(),
                            Map.Entry.comparingByValue()).getKey());
            List<Instruction> instructions = d.getInstructionsToGoTo(closestDestination);
            q.addInstructionsToQueue(instructions);
            reserveParcelsAtVertex(closestDestination, q, loadableParcels); // reserve parcels.
        }
    }


    private void reserveParcelsAtVertex(Vertex vertex, Train q, List<Parcel> availableParcels)  {
        List<Parcel> availableParcelsSorted = availableParcels.stream()
                .filter(p -> p.getLocation().equals(vertex))
                .filter(Parcel::stillNotDelivered)
                .filter(p -> p.canBeLoadedBy(q))
                .sorted(Comparator.comparingInt(Parcel::getDistanceToDestination).reversed()) // furthest distance
                .collect(Collectors.toList());
        if (availableParcelsSorted.size() == 0) {
            return;
        }
        Set<Parcel> parcelsToBePickedUp = new HashSet<>();
        Parcel furthestParcel = availableParcelsSorted.get(0);
        // compute the instructions to deliver this furthestParcel.
        Vertex source = (Vertex) furthestParcel.getLocation();
        Vertex destination = furthestParcel.getDestination();
        Dijkstra d = new Dijkstra(graph, source, destination);
        List<Instruction> instructions = d.getInstructionsToGoTo(destination);
        int n = instructions.size();
        Vertex v = destination;
        int accumulatedWeight = 0;
        for (int i = n - 1; i >= 0; i--) {
            Vertex finalV = v;
            List<Parcel> parcelsAtV = availableParcelsSorted.stream()
                    .filter(p -> Objects.equals(finalV, p.getDestination()))
                    .sorted(Comparator.comparingInt(Parcel::getWeight).reversed()) // heaviest gets priority
                    .collect(Collectors.toList());
            // attempt to load parcel here. Train will be empty when the beginning of this list of instructions is
            // executed.

            for (Parcel p : parcelsAtV) {
                if (accumulatedWeight + p.getWeight() <= q.getCapacity()) { // load p if possible.
                    parcelsToBePickedUp.add(p);
                    accumulatedWeight += p.getWeight();
                    instructions.get(0).getLoad().add(p);
                    instructions.get(i).getDrop().add(p);
                }
            }
            if (accumulatedWeight == q.getCapacity()) {
                break;
            }
            Edge e = instructions.get(i).getEdge();
            v = e.getOtherEnd(v);
        }
        q.addInstructionsToQueue(instructions);
        for (Parcel p : parcelsToBePickedUp) {
            p.reserveFor(q);
        }
    }

}
