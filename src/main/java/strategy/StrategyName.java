package strategy;

import graph.Graph;
import graph.Parcel;
import train.Train;

import java.util.List;

public enum StrategyName {
    STRATEGY_BASIC,
    STRATEGY1;

    public Strategy of(Graph graph, List<Parcel> parcels, List<Train> trains) {
        switch (this) {
            case STRATEGY1:
                return new Strategy1(graph, parcels, trains);
            case STRATEGY_BASIC:
            default:
                return new StrategyBasic(graph, parcels, trains);
        }
    }
}
