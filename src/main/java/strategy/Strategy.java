package strategy;

/**
 * A template for developing multiple strategies. The method assignJobs() is called by the Controller.
 */
public interface Strategy {
    void assignJobs();
}
