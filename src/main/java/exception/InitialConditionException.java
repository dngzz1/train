package exception;

public class InitialConditionException extends MyException {
    public InitialConditionException(String errorMessage) {
        super(errorMessage);
    }

    public InitialConditionException(String errorMessage, Throwable err) {
        super(errorMessage, err);
    }
}
