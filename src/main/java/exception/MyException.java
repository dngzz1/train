package exception;

public class MyException extends Exception{
    public MyException(String errorMessage) {
        super(errorMessage);
    }
    public MyException(String errorMessage, Throwable err) {
        super(errorMessage, err);
    }
}
