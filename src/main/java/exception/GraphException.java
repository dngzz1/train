package exception;

public class GraphException extends InitialConditionException {
    public GraphException(String errorMessage) {
        super(errorMessage);
    }

    public GraphException(String errorMessage, Throwable err) {
        super(errorMessage, err);
    }
}
