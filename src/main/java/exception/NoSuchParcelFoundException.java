package exception;

public class NoSuchParcelFoundException extends MyException {
    public NoSuchParcelFoundException(String errorMessage) {
        super(errorMessage);
    }
    public NoSuchParcelFoundException(String errorMessage, Throwable err) {
        super(errorMessage, err);
    }
}
