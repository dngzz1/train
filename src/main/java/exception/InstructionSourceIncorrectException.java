package exception;

public class InstructionSourceIncorrectException extends MyException {
    public InstructionSourceIncorrectException(String errorMessage) {
        super(errorMessage);
    }

    public InstructionSourceIncorrectException(String errorMessage, Throwable err) {
        super(errorMessage, err);
    }
}
