package exception;

public class TrainOverloadException extends MyException {
    public TrainOverloadException(String errorMessage) {
        super(errorMessage);
    }
    public TrainOverloadException(String errorMessage, Throwable err) {
        super(errorMessage, err);
    }
}
