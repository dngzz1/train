package train;

import graph.Edge;
import graph.Parcel;
import graph.Vertex;

import java.util.HashSet;
import java.util.Set;

/**
 * <h1>Instruction</h1>
 * An instance of an instruction can be passed into a train for execution.
 * One instruction is one move along an edge, possibly with loading at the source vertex and dropping at the target
 * vertex.
 * It contains the following information:
 * <ul>
 *     <li>the source vertex</li>
 *     <li>a set of parcels to load at the source</li>
 *     <li>the target vertex</li>
 *     <li>a set of parcels to deload at the target</li>
 *     <li>the edge along which the train plans to move (to take in account of multi-edges between the same two
 *     vertices)</li>
 * </ul>
 * The toString method is in the format required by the project.
 */
public class Instruction {
    private final Vertex source;
    private final Set<Parcel> load;
    private final Set<Parcel> drop;
    private final Edge edge;
    private final Vertex target;

    /**
     * An example of a builder method.
     */
    public static class Builder {
        // Required parameters
        private final Vertex vertex;
        private final Edge moving;
        // Optional parameters
        private Set<Parcel> load = new HashSet<>();
        private Set<Parcel> drop = new HashSet<>();
        // Constructor
        public Builder(Vertex vertex, Edge moving) {
            this.vertex = vertex;
            this.moving = moving;
        }
        public Builder load(Set<Parcel> val) {
            load = val;
            return this;

        }
        public Builder drop(Set<Parcel> val) {
            drop = val;
            return this;
        }
        public Instruction build() {
            return new Instruction(this);
        }
    }
    private Instruction(Builder builder) {
        source = builder.vertex;
        load = builder.load;
        drop = builder.drop;
        edge = builder.moving;
        target = edge.getOtherEnd(source);
    }

    /**
     * Returns the starting vertex of the instruction.
     * @return the starting vertex of the instruction.
     */
    public Vertex getSource() {
        return source;
    }

    /**
     * Returns the parcels to be loaded.
     * @return the parcels to be loaded.
     */
    public Set<Parcel> getLoad() {
        return load;
    }

    /**
     * Returns the parcels to be dropped.
     * @return the parcels to be dropped.
     */
    public Set<Parcel> getDrop() {
        return drop;
    }

    /**
     * Returns the edge in the instruction.
     * @return the edge in the instruction.
     */
    public Edge getEdge() {
        return edge;
    }

    /**
     * Returns the target vertex of the instruction.
     * @return the target vertex of the instruction.
     */
    public Vertex getTarget() {
        return target;
    }



    @Override
    public String toString() {
        return "load = " +
                load +
                ", drop = " +
                drop +
                ", moving " +
                source +
                "->" +
                target +
                ":" +
                edge.getName();
    }
}
