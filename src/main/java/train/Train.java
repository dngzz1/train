package train;

import controller.Controller;
import exception.InstructionSourceIncorrectException;
import exception.MyException;
import exception.NoSuchParcelFoundException;
import exception.TrainOverloadException;
import graph.Edge;
import graph.Location;
import graph.Parcel;

import java.util.*;
import java.util.stream.Collectors;

/**
 * <h1>Train data structure</h1>
 * A train contains the following information:
 * <ul>
 *     <li>its name</li>
 *     <li>its location (can be a vertex or an edge)</li>
 *     <li>its capacity, a non-negative integer.</li>
 *     <li>its controller. This is set by init() in Controller.</li>
 *     <li>a list of parcels on board.</li>
 *     <li>a current instruction and a queue on awaiting instructions.</li>
 *     <li>an expected arrival time (only relevant when train is carrying parcels). </li>
 * </ul>
 */
public class Train implements Location {
    private final String name;
    private Location location;
    private final int capacity;

    private Controller controller = null;
    private final List<Parcel> parcelsOnBoard = new ArrayList<>();
    private final Queue<Instruction> instructionQueue = new LinkedList<>();
    private Instruction currentInstruction = null;
    private int expectedArrivalTime = 0;

    /**
     * Constructor for a train.
     * @param name the name of the train.
     * @param location the starting location of the train.
     * @param capacity the capacity of the train.
     */
    public Train(String name, Location location, int capacity) {
        this.name = name;
        this.location = location;
        this.capacity = capacity;
    }

    /**
     * The train reads the instructions and executes one time step.
     */
    public void step()  {
        if (!isIdle()) {
            try {
                executeInstruction();
            } catch (MyException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Adds a list of instructions to the queue.
     * @param instructions the list of instructions to be added.
     */
    public void addInstructionsToQueue(List<Instruction> instructions) {
        for (Instruction instruction: instructions) {
            addInstructionToQueue(instruction);
        }
    }

    /**
     * Adds one instruction to the queue. If the instructionQueue is empty, then the instruction will go
     * straight to currentInstruction, bypassing the queue.
     * Helper method.
     * @param instruction the instruction to be added.
     */
    private void addInstructionToQueue(Instruction instruction) {
        if (currentInstruction == null) {
            currentInstruction = instruction;
            return;
        }
        instructionQueue.offer(instruction);
    }

    /**
     * Execute the current instruction if it exists, else set it to null.
     * @throws MyException if the instruction is invalid, e.g. loading a parcel that is not there.
     */
    void executeInstruction() throws MyException {
        if (currentInstruction == null) { return; } // sanity check.
        if (location instanceof Edge) {
            expectedArrivalTime--;
            if (expectedArrivalTime == 0) { // if arrived...
                unpackOnArrival();
            }
        } else { // if at a vertex...
            if (!currentInstruction.getSource().equals(location)) { // if instruction invalid...
                throw new InstructionSourceIncorrectException(name + ": Instruction source is "
                        + currentInstruction.getSource() + " but I am at " + location);
            }
            // else instruction valid.
            reportToController();

            load(currentInstruction.getLoad());
            expectedArrivalTime = currentInstruction.getEdge().getDistance();
            if (expectedArrivalTime == 1) { // if eta is 1 second, no need to go on edge.
                expectedArrivalTime = 0;
                unpackOnArrival();
            } else {
                location = currentInstruction.getEdge();
                expectedArrivalTime--;
            }
        }
    }

    /**
     * Helper method for executeInstruction().
     * Prints the string to the controller's log, which will be displayed after the algorithm is finished.
     */
    private void reportToController() {
        if (controller != null) {
            controller.getLog().append("@").append(controller.getTime())
                    .append(", q = ").append(name)
                    .append(", load(@").append(currentInstruction.getSource())
                    .append(") = ").append(currentInstruction.getLoad()).append(", ")
                    .append("moving ").append(currentInstruction.getSource())
                    .append("->")
                    .append(currentInstruction.getTarget())
                    .append(":").append(currentInstruction.getEdge().getName())
                    .append(", drop(@").append(currentInstruction.getTarget())
                    .append(") = ").append(currentInstruction.getDrop())
                    .append(", arriving @")
                    .append(controller.getTime() + currentInstruction.getEdge().getDistance())
                    .append("\n");
        }
    }

    /**
     * Helper method for executeInstruction().
     * Drops the parcels and takes new instructions if there is one in the queue, otherwise poll() returns null.
     * @throws NoSuchParcelFoundException sanity check to see if parcel exists on the train.
     */
    private void unpackOnArrival() throws NoSuchParcelFoundException {
        location = currentInstruction.getTarget();
        dropAtDestination(currentInstruction.getDrop());
        currentInstruction = instructionQueue.poll();
    }

    /**
     * Loads the parcels onto the train.
     * @param parcelsToBeLoaded parcels to be loaded.
     * @throws MyException if parcel not found then an exception is thrown.
     */
    private void load(Collection<Parcel> parcelsToBeLoaded) throws MyException {
        for (Parcel parcel : parcelsToBeLoaded) {
            if (!parcel.getLocation().equals(location)) { // check that the parcels exist.
                throw new NoSuchParcelFoundException(name + ": Parcel " + parcel + " not found at location " + location);
            }
        }
        int totalWeightToBeLoaded = parcelsToBeLoaded.stream().map(Parcel::getWeight).reduce(0, Integer::sum);
        if (totalWeightToBeLoaded + getWeightOfParcelsOnBoard() > capacity) { // check weight capacity.
            currentInstruction = null;
            throw new TrainOverloadException(name + ": cannot load parcels");
        }
        for (Parcel parcel : parcelsToBeLoaded) {
            parcel.setLocation(this);
            parcelsOnBoard.add(parcel);
        }
    }

    /**
     * Drops a parcel from the train to the parcel's destination.
     * @param parcel the parcel to be dropped.
     * @throws NoSuchParcelFoundException if parcel not found on train.
     */
    private void dropAtDestination(Parcel parcel) throws NoSuchParcelFoundException {
        if(!parcelsOnBoard.contains(parcel)) {
            throw new NoSuchParcelFoundException(name + ": Parcel " + parcel.getName()
                    + " is missing on board hence cannot be dropped.");
        }
        parcel.setLocation(this.location);
        parcel.setDistanceToDestination(0);
        parcelsOnBoard.remove(parcel);
    }

    /**
     * Drops several parcels from the train
     * @param parcels the parcels to be dropped
     * @throws NoSuchParcelFoundException if any of the parcels not found on train.
     */
    private void dropAtDestination(Collection<Parcel> parcels) throws NoSuchParcelFoundException {
        for (Parcel parcel : parcels) {
            dropAtDestination(parcel);
        }
    }


    // Getters and setters.

    /**
     * Returns the capacity of the train.
     * @return the capacity of the train.
     */
    public int getCapacity() {
        return capacity;
    }

    /**
     * A method that returns a set of parcels loadable by the current train.
     * @param parcels a list of parcels to consider.
     * @return a set of parcels loadable by q.
     *      * This filters for: reachability and capacity.
     *      * Parcels which have been successfully delivered are also not loadable.
     */
    public List<Parcel> getLoadableParcels(List<Parcel> parcels) {
        //noinspection Convert2MethodRef
        return parcels.stream()
                .filter(p -> p.isReachableBy(this))
                .filter(p -> p.stillNotDelivered())
                .filter(p -> p.canBeLoadedBy(this))
                .collect(Collectors.toList());
    }

    /**
     * Get the location of the train.
     * @return the location.
     */
    public Location getLocation() {
        return location;
    }

    /**
     * Gets the set of parcels on board.
     * @return the set of parcels on board.
     */
    public List<Parcel> getParcelsOnBoard() {
        return parcelsOnBoard;
    }

    /**
     * Returns the weight of parcels on board.
     * @return the weight of parcels on board.
     */
    public int getWeightOfParcelsOnBoard() {
        int total = 0;
        for (Parcel p : parcelsOnBoard) {
            total += p.getWeight();
        }
        return total;
    }

    /**
     * The train is idle if it currently has no instructions.
     * @return true if the train has no instructions.
     */
    public boolean isIdle() {
        return currentInstruction == null;
    }

    /**
     * Sets the controller of the train so that the train has access to the controller's log and time.
     * This method should really only be called by the controller.
     * @param controller the controller.
     */
    public void setController(Controller controller) {
        this.controller = controller;
    }

    @Override
    public String toString() {
        return name;
    }



}
