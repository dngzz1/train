package controller;

import exception.InitialConditionException;
import graph.*;
import train.Train;

import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <h1>Checker class</h1>
 * This class checks that the triple (graph, parcels, trains) is a valid configuration.
 * If not, it will throw an InitialConditionException.
 * Ideally we would have a single-responsibility class, but unfortunately the Initializer class also checks for a
 * similar kind of data structure problem as it does need to construct these objects and then pass them to the
 * Controller.
 */
public class Checker {
    public final Graph graph;
    public final List<Parcel> parcels;
    public final List<Train> trains;

    public Checker(Graph graph, List<Parcel> parcels, List<Train> trains) throws InitialConditionException {
        this.graph = graph;
        this.parcels = parcels;
        this.trains = trains;
        checkVerticesAtLeastTwo();
        checkEdgesCorrectlyConfigured();
        checkParcelsCorrectlyConfigured();
        checkTrainCorrectlyConfigured();
        checkAllParcelsDeliverable();
    }
    
    private void checkVerticesAtLeastTwo() throws InitialConditionException {
        if (graph.getVertices().size() < 2) {
            throw new InitialConditionException("Number of stations must be at least 2.");
        }
    }
    
    private void checkEdgesCorrectlyConfigured() throws InitialConditionException {
        Collection<Edge> edges = graph.getEdges();
        for (Edge edge : edges) {
            for (Vertex end : edge.getEnds()) {
                if(!(graph.getVertices().contains(end))) {
                    throw new InitialConditionException("Edge " + edge + " is incorrectly configured.");
                }
            }
        }
    }
    
    private void checkParcelsCorrectlyConfigured() throws InitialConditionException {
        for (Parcel parcel : parcels) {
            Vertex location = (Vertex) parcel.getLocation();
            Vertex destination = parcel.getDestination();
            if(!(graph.getVertices().contains(location)
                    && graph.getVertices().contains(destination))) {
                throw new InitialConditionException("Parcel " + parcel + " is incorrectly configured.");
            }
            if(parcel.getWeight() <= 0) {
                throw new InitialConditionException("Parcel " + parcel + " has invalid weight.");
            }
        }
    }

    private void checkTrainCorrectlyConfigured() throws InitialConditionException {
        if (trains.size() == 0) {
            throw new InitialConditionException("No trains were found.");
        }
        for (Train train : trains) {
            if (train.getCapacity() < 0) {
                throw new InitialConditionException("Train " + train + " has negative capacity.");
            }
        }
    }

    /**
     * This method checks that there are no parcels that are too heavy for any train to carry.
     * @throws InitialConditionException throws error if not all parcels are deliverable.
     */
    private void checkAllParcelsDeliverable() throws InitialConditionException {
        if (parcels.size() == 0) {
            return; // vacuously true if there are no parcels (algorithm should say job complete immediately).
        }
        outer:
        for (Parcel p : parcels) {
            // filter down to the set of trains with enough capacity.
            Set<Train> validTrains = trains.stream()
                    .filter(q -> q.getCapacity() >= p.getWeight())
                    .collect(Collectors.toSet());
            p.getValidTrains().addAll(validTrains);
            Set<Vertex> verticesWithValidTrains = validTrains.stream()
                    .map(Train::getLocation)
                    .map(location -> (Vertex) location)
                    .collect(Collectors.toSet());
            Dijkstra d = new Dijkstra(graph, (Vertex) p.getLocation(), verticesWithValidTrains);
            Collection<Integer> distancesWithValidTrain = d.getPathLengths().values();
            for(Integer distance : distancesWithValidTrain) {
                if (!distance.equals(Integer.MAX_VALUE)) {
                    continue outer;
                }
            }
            throw new InitialConditionException("There is a parcel unreachable by any valid train.");
        }
    }
}
