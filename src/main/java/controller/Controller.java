package controller;

import exception.InitialConditionException;
import graph.*;
import main.Initializer;
import strategy.Strategy;
import strategy.StrategyBasic;
import strategy.StrategyName;
import train.Train;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.groupingBy;
/**
 * <h1>Controller class</h1>
 * The controller class reads in relevant data from a file.
 * The method step() iterates the algorithm by 1 second.
 * The method run() iterates until the task is completed.
 */
public final class Controller {
    private final Graph graph;
    private final List<Parcel> parcels;
    private final List<Train> trains;
    private final Strategy strategy;
    private final StringBuilder log = new StringBuilder();
    private boolean allJobsFinished = false;
    private int time = 0;



    /**
     * Constructor for the controller.
     * @param filename the filename of the .txt file.
     * @param strategyName the name of the class of the strategy (an enum) that the controller will use the direct the
     *                     trains. This is a factory method.
     * @throws InitialConditionException if the text file is wrongly configured.
     */
    public Controller(String filename, StrategyName strategyName) throws InitialConditionException {
        Initializer initializer = new Initializer(filename); // this throws InitialConditionException
        graph = initializer.getGraph();
        parcels = initializer.getParcels();
        trains = initializer.getTrains();
        strategy = strategyName.of(graph, parcels, trains);
        init(); // this may throw InitialConditionException
    }

    /**
     * An alternative way to instantiate a controller. If we don't want to do it by .txt, we can directly pass the
     * data structures in.
     * @param graph the graph in question
     * @param parcels a list of parcels
     * @param trains a list of trains
     * @throws InitialConditionException exception
     */
    public Controller(Graph graph, List<Parcel> parcels, List<Train> trains) throws InitialConditionException {
        this.graph = graph;
        this.parcels = parcels;
        this.trains = trains;
        strategy = new StrategyBasic(graph, parcels, trains);
        init(); // this may throw InitialConditionException
    }

    /**
     * After (graph, parcels, trains) have been loaded, init() checks for configuration errors, makes some initial
     * calculations and passes this instance into the trains so that they can report to this controller.
     * @throws InitialConditionException exception
     */
    private void init() throws InitialConditionException {
        new Checker(graph, parcels, trains); // this may throw InitialConditionException
        calculateParcelDistances();
        for (Train train : trains) {
            train.setController(this);
        }
    }

    /**
     * This calculates the distance between the source and the destination of every parcel.
     * This algorithm takes advantage of the fact that the Dijkstra algorithm is more efficient when you iterate
     * through the pairs (source, targets) for each source, rather than the pairs (source,target) for each parcel.
     */
    private void calculateParcelDistances() {
        // Check that parcel source and destination are connected by a path.
        // Also calculate the distance.
        Map<Location, List<Parcel>> parcelsByLocation = parcels.stream()
                .collect(groupingBy(Parcel::getLocation));
        for(Location v : parcelsByLocation.keySet()) {
            List<Parcel> parcelsAtV = parcelsByLocation.get(v);
            List<Vertex> destinations = parcelsAtV.stream().map(Parcel::getDestination).collect(Collectors.toList());
            Dijkstra d = new Dijkstra(graph, (Vertex) v, destinations);
            Map<String, Integer> l = d.getPathLengths();
            for (Parcel p : parcelsAtV) {
                p.setDistanceToDestination(l.get(p.getDestination().getName()));
            }
        }
    }

    /**
     * Run until all jobs are finished. Run this function in main.
     */
    public void run() {
        while (!isAllJobsFinished()) {
            step();
        }
    }

    /**
     * Call this to iterate one step.
     * If all jobs are done then nothing will happen.
     */
    public void step() {
        if (isAllJobsFinished())  return;
        if (jobIsDoneForTheFirstTime()) return;
        strategy.assignJobs();
        for (Train train : trains) {
            train.step();
        }
        time++;
    }

    /**
     * This method checks whether all parcels have been delivered.
     * If so, set allJobsFinished to be true and return true.
     * If not, return false.
     * @return true if all jobs are finished and false otherwise.
     */
    private boolean jobIsDoneForTheFirstTime() {
        if (parcels.stream().filter(Parcel::stillNotDelivered).collect(Collectors.toSet()).size() == 0) {
            getLog().append("@").append(time).append(", all parcels delivered!");
            allJobsFinished = true;
            return true;
        }
        return false;
    }

    // Getters.

    /**
     * Allow anyone with access to this controller to look at the current time.
     * @return the current time
     */
    public int getTime() {
        return time;
    }

    public StringBuilder getLog() {
        return log;
    }

    public boolean isAllJobsFinished() {
        return allJobsFinished;
    }
}
