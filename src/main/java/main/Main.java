package main;

import controller.Controller;
import exception.InitialConditionException;
import strategy.StrategyName;

/**
 * Main entry point of the program.
 * The first argument is the file directory of the input text file.
 * The second argument is the name of the algorithm employed. Currently, there are only 2 strategies: StrategyBasic
 * and Strategy1.
 */
public class Main {
    public static void main(String[] args) {
        MainRunner.run();
    }

}
