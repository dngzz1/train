package main;

import controller.Controller;
import exception.InitialConditionException;
import strategy.StrategyName;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Main Runner is a utility class that reads files from the input directory and writes files to the output directory.
 */
public class MainRunner {

    /**
     * Reads files from the input directory and writes files to the output directory.
     * If there is a MyException from controller, then print it to the text file.
     */
    public static void run() {
        String dir = "src/main/resources/init/input/";
        Set<String> fileNames = listFilesUsingJavaIO(dir);
        for (String fileName : fileNames) {
            try {
                FileWriter myWriter = new FileWriter("src/main/resources/init/output/" + fileName);
                try {
                    Controller controller = new Controller(dir + fileName, StrategyName.STRATEGY1);
                    controller.run();
                    myWriter.write(controller.getLog().toString());
                    myWriter.close();
                } catch (InitialConditionException e) {
                    myWriter.write(e.getMessage());
                    myWriter.close();
                }
            } catch (IOException e) {
                e.printStackTrace();

            }
        }


    }

    /**
     * Gets a list of files from a directory
     * @param dir the directory
     * @return a set of strings containing filenames.
     */
    public static Set<String> listFilesUsingJavaIO(String dir) {
        return Stream.of(Objects.requireNonNull(new File(dir).listFiles()))
                .filter(file -> !file.isDirectory())
                .map(File::getName)
                .filter(f -> f.endsWith("txt"))
                .collect(Collectors.toSet());
    }
}
