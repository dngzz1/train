package main;


import exception.InitialConditionException;
import graph.Edge;
import graph.Graph;
import graph.Parcel;
import graph.Vertex;
import train.Train;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

/**
 *  <h1>Initialize data from txt</h1>
 *  The Initializer is a helper class for Controller that loads the data from the text file into the relevant data
 *  structures. The motivation for writing a separate class is so that the code for the Scanner object does not
 *  obfuscate the rest of the code in Controller.
 *
 *  This class also checks for infeasible set ups.
 *  <ul>
 *      <li><b>Vertex: </b>The number of vertices must be at least 2. If there is only one vertex then there is no point
 *      delivering any parcels!</li>
 *      <li><b>Edge: </b>The edges must have vertices that are in the graph.</li>
 *      <li><b>Parcel: </b>The parcel source and location vertices must be in the graph.</li>
 *      <li><b>Train: </b> The number of trains must be at least 1.</li>
 *  </ul>
 *  This is to ensure that the data structures can be correctly set up (e.g. cannot have -1 vertices).
 * @author    Daniel Ng
 *
 */
public class Initializer {
    private Graph graph;
    private final List<Parcel> parcels = new ArrayList<>();
    private final List<Train> trains = new ArrayList<>();

    /**
     * The constructor for the Initializer requires the file path of the text file.
     * @param filename e.g. "resources/init.txt"
     * @throws InitialConditionException if the data is not configured properly, the class will throw an
     * InitialConditionException.
     */
    public Initializer(String filename) throws InitialConditionException {
        List<Vertex> vertices = new ArrayList<>();
        List<Edge> edges = new ArrayList<>();
        try {
            File myObj = new File(filename);
            Scanner myReader = new Scanner(myObj);
            while (myReader.hasNextLine()) {
                String line = myReader.nextLine();

                if (line.contains("number of stations")) {
                    int numberOfStations = Integer.parseInt(line.replaceAll("([0-9]+) .*","$1"));
                    /*
                      require there to be at least 2 vertices, otherwise there is no point running an algorithm.
                     */
                    if (numberOfStations < 2) {
                        throw new InitialConditionException("Number of stations must be at least 2.");
                    }
                    for (int i = 0; i < numberOfStations; i++) {
                        String stationName = myReader.nextLine();
                        stationName = stationName.replaceAll("([A-Za-z0-9]+) .*", "$1");
                        vertices.add(new Vertex(stationName));
                    }
                }

                if (line.contains("number of routes")) {
                    int numberOfRoutes = Integer.parseInt(line.replaceAll("([0-9]+) .*","$1"));
                    for (int i = 0; i < numberOfRoutes; i++) {
                        String[] edgeData = myReader.nextLine().split(",");
                        String edgeName = edgeData[0];
                        String sourceName = edgeData[1];
                        List<Vertex> sourceWithSourceName =
                                vertices.stream()
                                        .filter(v->sourceName.equals(v.getName()))
                                        .collect(Collectors.toList());
                        if (sourceWithSourceName.size() == 0) {
                            throw new InitialConditionException("Edge " + edgeName + " is incorrectly configured.");
                        }
                        Vertex source = sourceWithSourceName.get(0);
                        String targetName = edgeData[2];
                        List<Vertex> targetWithTargetName = vertices.stream()
                                .filter(v->targetName.equals(v.getName()))
                                .collect(Collectors.toList());
                        if (targetWithTargetName.size() == 0) {
                            throw new InitialConditionException("Edge " + edgeName + " is incorrectly configured.");
                        }
                        Vertex target = targetWithTargetName.get(0);
                        edgeData[3] = edgeData[3].replaceAll("([0-9]+) .*","$1");
                        int distance = Integer.parseInt(edgeData[3]);
                        edges.add(new Edge(edgeName, source, target, distance));
                    }
                }

                graph = new Graph(vertices, edges); // may throw GraphException

                if (line.contains("number of deliveries to be performed")) {
                    int numberOfParcels = Integer.parseInt(line.replaceAll("([0-9]+) .*","$1"));
                    for (int i = 0; i < numberOfParcels; i++) {
                        String[] parcelData = myReader.nextLine().split(",");
                        String name = parcelData[0];
                        String locationName = parcelData[1];
                        Vertex location = getGraph().getVertex(locationName);
                        String destinationName = parcelData[2];
                        Vertex destination = getGraph().getVertex(destinationName);
                        parcelData[3] = parcelData[3].replaceAll("([0-9]+) .*","$1");
                        int weight = Integer.parseInt(parcelData[3]);

                        /*
                         * Check that the source and destination of the parcel exist in the graph.
                         */
                        if(!(getGraph().getVertices().contains(location)
                                && getGraph().getVertices().contains(destination))) {
                            throw new InitialConditionException("Parcel " + name + " is incorrectly configured.");
                        }
                        getParcels().add(new Parcel(name, location, destination, weight));
                    }
                }

                if (line.contains("number of trains")) {
                    int numberOfTrains = Integer.parseInt(line.replaceAll("([0-9]+) .*","$1"));
                    /*
                     * Check that there is at least one train.
                     */
                    if (numberOfTrains <= 0) {
                        throw new InitialConditionException("No trains were found.");
                    }
                    for (int i = 0; i < numberOfTrains; i++) {
                        String[] trainData = myReader.nextLine().split(",");
                        String name = trainData[0];
                        String locationName = trainData[1];
                        Vertex location = getGraph().getVertex(locationName);
                        trainData[2] = trainData[2].replaceAll("([0-9]+) .*","$1");
                        int capacity = Integer.parseInt(trainData[2]);
                        getTrains().add(new Train(name, location, capacity));
                    }

                }

            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("Invalid file format.");
            e.printStackTrace();
        }
    }

    public List<Parcel> getParcels() {
        return parcels;
    }

    public List<Train> getTrains() {
        return trains;
    }

    public Graph getGraph() {
        return graph;
    }
}

