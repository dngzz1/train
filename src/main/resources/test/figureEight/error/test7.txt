// figure of 8 graph from 
// https://www.freecodecamp.org/news/dijkstras-shortest-path-graph-visual-introduction/
7 // number of stations
V0 // station name
V1 // station name
V2 // station name
V3 // station name
V4 // station name
V5 // station name
V6 // station name
9 // number of routes
E1,V0,V1,2
E2,V0,V2,6
E3,V1,V3,5
E4,V2,V3,8
E5,V3,V4,10
E6,V3,V5,15
E7,V4,V5,6
E8,V4,V6,2
E9,V5,V6,6
2 // number of deliveries to be performed
P1,V0,V6,5
P2,V0,V6,5 
1 // number of trains
Q1,V6,-5 // negative capacity!
