// figure of 8 graph from 
// https://www.freecodecamp.org/news/dijkstras-shortest-path-graph-visual-introduction/
1 // number of stations
V0 // station name

0 // number of routes

1 // number of deliveries to be performed
P1,V0,V6,5
1 // number of trains
Q1,V0,100
